import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "antd/dist/antd.css";
import "./index.module.scss";

import { ConfigProvider } from "antd";
import pt_BR from "antd/lib/locale-provider/pt_BR";
import moment from "moment";
import "moment/locale/pt-br";

moment.locale("pt-BR");

ReactDOM.render(
  <React.StrictMode>
    <ConfigProvider locale={pt_BR}>
      <App />
    </ConfigProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
