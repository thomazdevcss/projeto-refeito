import React from "react";
import Tabela from "./Pages/Tabela/Tabela";
import ProdutoDetalhe from "./Pages/ProdutoDetalhe/ProdutoDetalhe";
import NotFound from "./Pages/NotFound/NotFound";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/produto/detalhe/:id" component={ProdutoDetalhe} />
          <Route exact path="/" component={Tabela} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
