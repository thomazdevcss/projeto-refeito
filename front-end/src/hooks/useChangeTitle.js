import React, { useState, useEffect } from "react";

export default function useChangeTitle(string) {
  const [title, setTitle] = useState("");
  if (title) setTitle(string);
  useEffect(() => {
    document.title = string;
  }, [title]);

  return title;
}
