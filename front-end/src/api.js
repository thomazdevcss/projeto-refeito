import axios from "axios";

const http = axios.create({
  baseURL: "https://localhost:5001/api"
});

export const api = {
  getProdutos() {
    return http.get("/produto");
  },
  getProdutoPorId(id) {
    return http.get(`/produto/${id}`);
  },
  postProduto(produto) {
    return http.post("/produto", produto);
  },
  putProduto(produto) {
    return http.put(`/produto/`, produto);
  },
  deleteProduto(id) {
    return http.delete(`/produto/${id}`);
  }
};
