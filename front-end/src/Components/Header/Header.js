import React, { useState } from "react";
import { header } from "./Header.module.scss";
export default function Header({ children, titulo }) {
  return (
    <div>
      <header className={header}>
        <h1>{titulo}</h1>
        {children}
      </header>
    </div>
  );
}
