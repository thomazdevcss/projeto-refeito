import React, { useState, useEffect } from "react";
import { api } from "../../api";
import { Button, message, Divider } from "antd";
import moment from "moment";
import { formProduto } from "./CadastroProduto.module.scss";
import {
  DatePickerPadrao,
  InputNumberPadrao,
  InputPadrao
} from "../../ComponentesPadrao";
import TextArea from "antd/lib/input/TextArea";

const INITIAL_STATE = {
  nome: "",
  descricao: "",
  imagem: "",
  valor: "",
  dataVencimento: "",
  observacao: ""
};

export default function CadastroProduto({ produtoAlterar, onCancel, onOk }) {
  const [produto, setProduto] = useState(INITIAL_STATE);

  useEffect(() => {
    setProduto({ ...INITIAL_STATE, ...produtoAlterar });
  }, [produtoAlterar]);

  async function inserirProduto() {
    setload(true);
    await api
      .postProduto(produto)
      .then(res => {
        message.success("Produto inserido com sucesso");
        console.log("Produto inserido com sucesso");
        onOk({ ...produto, id: res.data.id });
      })
      .catch(err => {
        message.error(err.message);
      });
    setload(false);
  }

  async function AlterarProduto() {
    setload(true);
    await api
      .putProduto(produto)
      .then(() => {
        console.log("Produto alterado com sucesso");
        message.success("Produto alterado com sucesso");
      })
      .catch(err => {
        console.log(err.message);
        message.error(err.message);
      });
    onOk(produto);
    setload(false);
  }

  const [load, setload] = useState(false);
  return (
    <form className={formProduto}>
      <div>
        <label htmlFor="nome">Nome</label>
        <InputPadrao
          value={produto.nome}
          type="text"
          id="nome"
          value={produto.nome}
          onChange={e => {
            setProduto({ ...produto, nome: e.target.value });
          }}
        />
      </div>

      <div>
        <label htmlFor="descricao">Descricão</label>
        <TextArea
          id="descricao"
          maxLength={200}
          style={{ resize: "none" }}
          value={produto.descricao}
          onChange={e => {
            setProduto({ ...produto, descricao: e.target.value });
          }}
        />
      </div>

      <div>
        <label htmlFor="valor">Valor</label>
        <InputNumberPadrao
          id="valor"
          style={{ width: "100%" }}
          value={produto.valor}
          onChange={value => {
            setProduto({ ...produto, valor: value });
          }}
        />
      </div>

      <div>
        <label htmlFor="imagem">Imagem</label>
        <InputPadrao
          id="imagem"
          value={produto.imagem}
          onChange={e => {
            setProduto({ ...produto, imagem: e.target.value });
          }}
        />
      </div>

      <div>
        <label htmlFor="observacao">Observação</label>
        <TextArea
          id="observacao"
          style={{ resize: "none" }}
          value={produto.observacao}
          maxLength={200}
          onChange={e => {
            setProduto({ ...produto, observacao: e.target.value });
          }}
        />
      </div>

      <div>
        <label>Data de validade</label>
        <DatePickerPadrao
          style={{ width: "100%" }}
          value={
            produto.dataVencimento
              ? moment(produto.dataVencimento, "DD-MM-YYYY")
              : moment()
          }
          onChange={(data, res) => {
            setProduto({ ...produto, dataVencimento: res });
          }}
        ></DatePickerPadrao>
      </div>
      <Divider style={{ margin: "10px 0" }} />
      <div style={{ textAlign: "right" }}>
        <Button key="back" onClick={onCancel} disabled={load}>
          Cancelar
        </Button>
        &nbsp;
        <Button
          key="submit"
          type="primary"
          style={{ background: "rgba(214, 48, 48, 1)", border: "none" }}
          onClick={() => {
            produto.id ? AlterarProduto() : inserirProduto();
          }}
          loading={load}
        >
          Inserir
        </Button>
      </div>
    </form>
  );
}
