import React from "react";
import { DatePicker, InputNumber, Input } from "antd";

export function DatePickerPadrao(props) {
  return <DatePicker format={"DD/MM/YYYY"} {...props} />;
}

export function InputNumberPadrao(props) {
  return (
    <InputNumber
      formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
      parser={value => value.replace(/\$\s?|(,*)/g, "")}
      min={0}
      max={1000000}
      {...props}
    />
  );
}

export function InputPadrao(props) {
  return <Input maxLength={500} {...props} />;
}
