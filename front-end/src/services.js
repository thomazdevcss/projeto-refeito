export const formatarDinheiro = valor => {
  if (Number.isNaN(valor)) return;

  return valor.toLocaleString("pt-BR", { style: "currency", currency: "BRL" });
};
