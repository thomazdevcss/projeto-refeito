import React, { useState, useEffect } from "react";
import { Popconfirm, message, Modal, Table, Tooltip } from "antd";
import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { api } from "../../api";
import Header from "../../Components/Header/Header";
import CadastroProduto from "../../Components/CadastroProduto/CadastroProduto";
import { formatarDinheiro } from "../../services";
import useChangeTitle from "../../hooks/useChangeTitle";

export default function Tabela() {
  const [listaProdutos, setListaProdutos] = useState(null);
  const [produto, setProduto] = useState(null);

  useChangeTitle("Produtos");

  useEffect(() => {
    buscarProdutos();
  }, []);

  async function buscarProdutos() {
    await api
      .getProdutos()
      .then(res => {
        setListaProdutos(res.data);
      })
      .catch(err => {
        console.log(err.message);
        message.error(err.message);
      });
  }

  async function deletarProduto(produtoId) {
    const id = listaProdutos.findIndex(x => x.id === produtoId);
    const currentProduct = produtoId;
    await api
      .deleteProduto(currentProduct)
      .then(() => {
        listaProdutos.splice(id, 1);
        setListaProdutos([...listaProdutos]);
        message.success("produto deletado com sucesso!");
      })
      .catch(err => {
        console.log(err);
        message.error(err.message);
      });
  }

  function compare(a, b) {
    let comparison = 0;
    if (a > b) {
      comparison = 1;
    } else if (a < b) {
      comparison = -1;
    }
    return comparison;
  }
  function sortPorNome() {
    setListaProdutos([...listaProdutos.sort(compare)]);
  }
  function alterarProduto(id) {
    const produtoAlterar = listaProdutos.filter(x => x.id === Number(id))[0];
    setProduto({ ...produto, ...produtoAlterar });
  }

  function adicionarProduto(prod) {
    if (produto.id) {
      const index = listaProdutos.findIndex(x => x.id === prod.id);
      listaProdutos[index] = { ...listaProdutos[index], ...prod };
      setListaProdutos(listaProdutos);
    } else {
      listaProdutos.push(prod);
    }
    setProduto(null);
  }

  return (
    <>
      <Header titulo="Produtos">
        <button onClick={() => setProduto({ id: 0 })}>Inserir produto</button>
      </Header>
      <div>
        <Modal
          visible={!!produto}
          onCancel={() => setProduto(null)}
          footer={null}
          width={600}
        >
          <CadastroProduto
            produtoAlterar={produto}
            onCancel={() => setProduto(null)}
            onOk={adicionarProduto}
          />
        </Modal>

        <Table
          pagination={{ pageSize: "8", style: { marginRight: "50%" } }}
          loading={!listaProdutos}
          style={{
            margin: "auto",
            marginTop: "15px",
            borderRadius: "10px"
          }}
          columns={[
            {
              title: "ID",
              dataIndex: "id",
              key: "id"
            },
            {
              title: "Nome",
              dataIndex: "nome",
              key: "nome",
              sortDirections: ["ascend"],
              sorter: (a, b) => {
                var nomeA = a.nome.toUpperCase();
                var nomeB = b.nome.toUpperCase();
                return nomeA < nomeB ? -1 : nomeA > nomeB ? 1 : 0;
              }
            },
            {
              title: "Valor",
              dataIndex: "valor",
              key: "valor"
            },
            { dataIndex: "detalhes", key: "detalhes" },
            { dataIndex: "alterar", key: "alterar" },
            { dataIndex: "deletar", key: "deletar" }
          ]}
          dataSource={
            listaProdutos
              ? listaProdutos.map((prod, index) => ({
                  key: index,
                  id: prod.id,
                  nome: prod.nome,
                  valor: formatarDinheiro(prod.valor),
                  detalhes: (
                    <Tooltip
                      placement="bottom"
                      title="Ver"
                      mouseEnterDelay="0.5"
                    >
                      <Link
                        to={`/produto/detalhe/${prod.id}`}
                        style={{ color: "#252525" }}
                      >
                        <SearchOutlined />
                      </Link>
                    </Tooltip>
                  ),
                  alterar: (
                    <Tooltip
                      placement="bottom"
                      title="Editar"
                      mouseEnterDelay="0.5"
                    >
                      <EditOutlined
                        style={{ cursor: "pointer" }}
                        onClick={() => alterarProduto(prod.id)}
                        value={prod.id}
                      />
                    </Tooltip>
                  ),
                  deletar: (
                    <Tooltip
                      placement="bottom"
                      title="Deletar"
                      mouseEnterDelay="0.5"
                    >
                      <Popconfirm
                        placement="top"
                        title="Deseja deletar?"
                        onConfirm={() => deletarProduto(prod.id)}
                        okText="sim"
                        cancelText="Não"
                      >
                        <DeleteOutlined style={{ cursor: "pointer" }} />
                      </Popconfirm>
                    </Tooltip>
                  )
                }))
              : []
          }
        />
      </div>
    </>
  );
}
