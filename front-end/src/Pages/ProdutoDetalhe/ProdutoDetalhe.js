import React, { useEffect, useState } from "react";
import Header from "../../Components/Header/Header";
import { api } from "../../api";
import { formatarDinheiro } from "../../services";
import { Link } from "react-router-dom";
import CadastroProduto from "../../Components/CadastroProduto/CadastroProduto";
import { Drawer, Skeleton, message } from "antd";
import useChangeTitle from "../../hooks/useChangeTitle";
import {
  background,
  produtoDetalhe,
  descricao,
  observacao
} from "./ProdutoDetalhes.module.scss";

export default function ProdutoDetalhe({ match, history }) {
  const [produto, setProduto] = useState(null);
  const [drawerVisible, setDrawerVisble] = useState(false);

  useChangeTitle("Detalhes");

  useEffect(() => {
    buscarProduto(match.params.id);
  }, []);

  async function buscarProduto(id) {
    await api
      .getProdutoPorId(id)
      .then(res => {
        setProduto(res.data);
      })
      .catch(err => {
        console.log(err.message);
        message.error("produto não foi encontrado");
        history.push("/");
      });
  }

  return (
    <div>
      <Header titulo={<Link to="/">Detalhes</Link>}>
        <button onClick={() => setDrawerVisble(true)}>Editar Produto</button>
      </Header>

      <div className={background}>
        <Drawer
          visible={drawerVisible}
          width={"50%"}
          onClose={() => setDrawerVisble(false)}
        >
          <CadastroProduto
            produtoAlterar={produto}
            onOk={prodAlterado => {
              setProduto({ ...produto, ...prodAlterado });
              setDrawerVisble(false);
            }}
            onCancel={() => setDrawerVisble(false)}
          ></CadastroProduto>
        </Drawer>
        <div className={produtoDetalhe}>
          <Skeleton loading={!produto} paragraph={{ width: 250 }} active />
          {produto && (
            <>
              <h1>{produto.nome}</h1>
              <img
                src={produto.imagem}
                alt={produto.nome}
                onError={() =>
                  setProduto({
                    ...produto,
                    imagem:
                      "https://sainfoinc.com/wp-content/uploads/2018/02/image-not-available.jpg"
                  })
                }
              />
              <div className={observacao}>
                <h2>Observação</h2>
                <p>{produto.observacao}</p>
              </div>
              <div className={descricao}>
                <h2>{formatarDinheiro(produto.valor)}</h2>
                <h2>Descrição</h2>
                <p>{produto.descricao}</p>
                <h2>Data de vencimento</h2>
                <p>{produto.dataVencimento}</p>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
}
