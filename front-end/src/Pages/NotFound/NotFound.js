import React from "react";
import Header from "../../Components/Header/Header";
import { Link } from "react-router-dom";
import useChangeTitle from "../../hooks/useChangeTitle";
export default function NotFound() {
  useChangeTitle("Not found");
  return (
    <div
      style={{
        width: "100%",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Header titulo={<Link to="/">Inicio</Link>} />
      <img
        src="https://i.ya-webdesign.com/images/image-not-found-png-8.png"
        alt="not found"
        style={{
          width: "50%",
          marginLeft: "25%",
          marginTop: "10%"
        }}
      />
    </div>
  );
}
