using System;
using System.Collections.Generic;
using backend.dominio.entidade;
using backend.Dominio.Entidade;
using backend.Dominio.Repositorio.Interface;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private IRepositorioProduto repositorioProduto;
        public ProdutoController(IRepositorioProduto repositorioProduto)
        {
            this.repositorioProduto = repositorioProduto;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Produto>> Listar()
        {
            return new OkObjectResult(this.repositorioProduto.ObterTodos());
        }
        [HttpGet("{id}")]
        public ActionResult<Produto> ObterPorId(int id)
        {
            try
            {
                return new OkObjectResult(this.repositorioProduto.ObterPorId(id));

            }
            catch (Exception erro)
            {
                return new BadRequestObjectResult(erro.Message);
            }
        }
        [HttpPost]
        public ActionResult Inserir([FromBody] Produto produto)
        {
            try
            {
                this.repositorioProduto.Inserir(produto);
                return new ObjectResult(produto);
            }
            catch (Exception erro)
            {
                return new BadRequestObjectResult(erro.Message);
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Deletar(int id)
        {
            try
            {
                this.repositorioProduto.Deletar(id);
                return new NoContentResult();
            }
            catch (Exception erro)
            {
                return new BadRequestObjectResult(erro.Message);
            }

        }
        [HttpPut]
        public ActionResult Alterar([FromBody] Produto produto)
        {
            try
            {
                this.repositorioProduto.Alterar(produto);
                return new NoContentResult();
            }
            catch (Exception erro)
            {
                return new BadRequestObjectResult(erro.Message);
            }

        }
    }
}