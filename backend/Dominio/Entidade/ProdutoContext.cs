using backend.dominio.entidade;
using Microsoft.EntityFrameworkCore;

namespace backend.Dominio.Entidade
{
    public class ProdutoContext : DbContext
    {
        public ProdutoContext(DbContextOptions options) : base(options) { }
        public DbSet<Produto> Produtos { get; set; }

    }
}