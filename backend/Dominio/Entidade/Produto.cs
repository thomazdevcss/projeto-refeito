using System;
using System.ComponentModel.DataAnnotations;

namespace backend.dominio.entidade
{
    public class Produto
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(150)]
        public string Nome { get; set; }
        [MaxLength(500)]
        public string Descricao { get; set; }
        public string Imagem { get; set; }
        [Required]
        public double Valor { get; set; }
        public string DataVencimento { get; set; }
        [MaxLength(500)]
        public string Observacao { get; set; }
    }
}