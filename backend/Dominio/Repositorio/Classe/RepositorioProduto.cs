using System.Collections.Generic;
using backend.dominio.entidade;
using backend.Dominio.Entidade;
using backend.Dominio.Repositorio.Interface;
using System.Linq;
using System;

namespace backend.Dominio.Repositorio.Classe
{
    public class RepositorioProduto : IRepositorioProduto
    {
        public RepositorioProduto(ProdutoContext produtoContext)
        {
            this.produtoContext = produtoContext;
        }
        private ProdutoContext produtoContext;
        public void Alterar(Produto produto)
        {
            if (produto.Id == 0)
            {
                throw new ArgumentException("Id não encontrado");
            }
            this.produtoContext.Produtos.Update(produto);
            this.produtoContext.SaveChanges();
        }

        public void Deletar(int id)
        {
            Produto produto = this.produtoContext.Produtos.Where(x => x.Id == id).FirstOrDefault();
            if (produto.Id == 0)
            {
                throw new ArgumentException("Id não encontrado");
            }
            this.produtoContext.Produtos.Remove(produto);
            this.produtoContext.SaveChanges();
        }

        public void Inserir(Produto produto)
        {
            this.produtoContext.Produtos.Add(produto);
            this.produtoContext.SaveChanges();
        }

        public Produto ObterPorId(int id)
        {
            Produto produto = this.produtoContext.Produtos.Where(x => x.Id == id).FirstOrDefault();
            if (produto.Id == 0)
            {
                throw new ArgumentException("Id não encontrado");
            }
            return produto;
        }

        public IEnumerable<Produto> ObterTodos()
        {
            return this.produtoContext.Produtos.ToList();
        }
    }
}