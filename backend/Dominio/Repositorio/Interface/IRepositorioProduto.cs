using System.Collections.Generic;
using backend.dominio.entidade;

namespace backend.Dominio.Repositorio.Interface
{
    public interface IRepositorioProduto
    {
        void Inserir(Produto produto);
        void Alterar(Produto produto);
        void Deletar(int id);
        IEnumerable<Produto> ObterTodos();
        Produto ObterPorId(int id);
    }
}