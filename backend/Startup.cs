using backend.Dominio.Entidade;
using backend.Dominio.Repositorio.Classe;
using backend.Dominio.Repositorio.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private string connection = "SistemaProdutosConexaoSqlServer";
        readonly string MyAllowSpecificOrigins = "myAllowSpecificOrigins";
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = this.Configuration.GetConnectionString(this.connection);
            services.AddDbContext<ProdutoContext>(options => options.UseSqlServer(connectionString));
            services.AddControllers();
            services.AddScoped<IRepositorioProduto, RepositorioProduto>();
            services.AddCors(options =>
          {
              options.AddPolicy(MyAllowSpecificOrigins,
              builder =>
              {
                  builder.WithOrigins()
                                      .AllowAnyHeader()
                                      .AllowAnyMethod()
                                      .AllowAnyOrigin();
              });
          });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                app.UseCors(MyAllowSpecificOrigins);
            });
        }
    }
}
